<?php

/**
 * LinkWithOtherReponse
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2021 Denis Chenu <www.sondages.pro> and contributors
 * @license AGPL v3
 * @version 0.3.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class LinkWithOtherReponse extends PluginBase
{

    protected static $description = 'Search other in other responses to create link between.';
    protected static $name = 'LinkWithOtherReponse';

    /** @var PluginEvent $questionRenderEvent*/
    private $questionRenderEvent;

    public function init()
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }
        $this->subscribe('beforeQuestionRender', 'setLinkWithOtherReponse');
        $this->subscribe('newQuestionAttributes', 'addLinkWithOtherReponseAttribute');
    }

    /**
    * Launch autocmplete for question
    */
    public function setLinkWithOtherReponse()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }

        if ($this->getEvent()->get('type') == "T") {
            $qid = $this->getEvent()->get('qid');
            $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
            if (!empty($aAttributes['LinkWithOtherReponseTitle'])) {
                $this->questionRenderEvent = $this->getEvent();
                $answer = $this->renderAnswer($qid);
                if ($answer) {
                    $this->questionRenderEvent->set('answers', $answer);
                }
            }
        }
    }

    /**
     * Return the answer for question
     * @param integer $qid
     * @param PluginEvent $event
     * @return null|string null if no update was to be done
     */
    private function renderAnswer($qid)
    {
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        $LinkWithOtherReponseTitle = $aAttributes['LinkWithOtherReponseTitle'];
        $oQuestion = Question::model()->find("qid = :qid", array(':qid' => $qid));
        $sid = $oQuestion->sid;
        $oSurvey = Survey::model()->findByPk($sid);
        $useToken = !$oSurvey->getIsAnonymized() && $oSurvey->getHasTokensTable();
        /* token */
        $token = null;
        if ($useToken && $aAttributes['LinkWithOtherReponseTokenUsage'] != 'no') {
            if (empty(App()->getRequest()->getParam('token')) && empty($_SESSION['survey_' . $sid]['token'])) {
                $message = "<div class='alert alert-warning'>" . $this->translate("Unable to show other response without token.") . "</div>";
                if($this->questionRenderEvent) {
                    $this->questionRenderEvent->set('text', $this->questionRenderEvent->get('text') . $message);
                }
                return;
            }
            $token = App()->getRequest()->getParam('token');
            if (empty($token)) {
                $token = $_SESSION['survey_' . $sid]['token'];
            }
        }
        /* Some specific data */
        $srid = null;
        if (!empty($_SESSION['survey_' . $sid]['srid'])) {
            $srid = $_SESSION['survey_' . $sid]['srid'];
        }
        /* Get the data */
        $aReponses = $this->getData($sid, $qid, $token, $srid);
        if (is_null($aReponses)) {
            return null;
        }

        $name = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        $coreClass = "ls-answers answer-item dropdown-item";
        $extraclass = "";
        if (trim($aAttributes['text_input_width']) != '') {
            $col = ($aAttributes['text_input_width'] <= 12) ? $aAttributes['text_input_width'] : 12;
            $extraclass .= " col-sm-" . trim($col);
            $withColumn = true;
        } else {
            $withColumn = false;
        }
        $currentValue = null;
        if (isset($_SESSION['survey_' . $sid][$name])) {
            $currentValue = $_SESSION['survey_' . $sid][$name];
            if ($aAttributes['LinkWithOtherReponseSaveAs'] == 'json') {
                $decodedValue = @json_decode($currentValue, 1);
                if (is_array($decodedValue)) {
                    if(!empty($decodedValue['id'])) {
                        $currentValue = $decodedValue['id'];
                    }
                }
            }
            
            if (!isset($aReponses[$currentValue])) {
                $currentValue = null;
            }
        }
        $renderData = array(
            'aSurveyInfo' => getSurveyInfo($sid, Yii::app()->getLanguage()),
            'name' => $name,
            'coreClass' => $coreClass,
            'extraclass' => $extraclass,
            'withColumn' => $withColumn,
            'aReponses' => $aReponses,
            'currentValue' => $currentValue,
            'dispVal' => CHtml::encode($currentValue),
            'aReponses' => $aReponses,
            'aQuestion' => $oQuestion->getAttributes(),
            'aAttributes' => QuestionAttribute::model()->getQuestionAttributes($qid),
            'saveas' => $aAttributes['LinkWithOtherReponseSaveAs']
        );
        $this->subscribe('getPluginTwigPath', 'getPluginTwigPath');
        $answer = Yii::app()->twigRenderer->renderPartial(
            '/survey/questions/answer/longfreetext/linkwithotheresponse/answer.twig',
            $renderData
        );
        $this->unsubscribe('getPluginTwigPath');
        return $answer;
    }

    /**
    * The attributes
    */
    public function addLinkWithOtherReponseAttribute()
    {
        $addLinkWithOtherReponseAttribute = array(
            'LinkWithOtherReponseTitle' => array(
                'types' => 'T',
                'category' => $this->translate('Link with responses'),
                'sortorder' => 100,
                'inputtype' => 'text',
                'default' => "",
                'caption' => $this->translate("Question code to be be used for list"),
                'help' => $this->translate("This option allow to link each response with other response in same survey."),
            ),
            'LinkWithOtherReponseTokenUsage' => array(
                'types' => 'T',
                'category' => $this->translate("Link with responses"),
                'sortorder' => 120, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    'no' => gT('No'),
                    'token' => gT('Yes'),
                    'group' => $this->translate('Token Group')
                ),
                'default' => 'token',
                'caption' => $this->translate('Usage of token.'),
            ),
            'LinkWithOtherReponseSubmitted' => array(
                'types' => 'T',
                'category' => $this->translate("Link with responses"),
                'sortorder' => 130, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'default' => 0,
                'caption' => $this->translate('Only submitted reponse.'),
                'help' => $this->translate('You can always check if submitted with <code>submitted</code> key.'),
            ),
            'LinkWithOtherReponseExtraFilter' => array(
                'types' => 'T',
                'category' => $this->translate("Link with responses"),
                'sortorder' => 140,
                'inputtype' => 'textarea',
                'default' => "",
                'expression' => 1,
                'help' => $this->translate('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>), you can use Expressiona Manager in value, for filter, you can use <code>&gt;</code>, <code>&lt;</code> and other comparators.'),
                'caption' => $this->translate('Extra filter for selection.'),
            ),
            'LinkWithOtherReponseOrderBy' => array(
                'types' => 'T',
                'category' => $this->translate("Link with responses"),
                'sortorder' => 150 , /* Own category */
                'inputtype' => 'text',
                'default' => "",
                'help' => $this->translate('Use expression question code for the columns to be ordered.'),
                'caption' => $this->translate('Default order by (default “id DESC”, “datestamp ASC” for datestamped surveys)'),
            ),
            'LinkWithOtherReponseSaveAs' => array(
                'types' => 'T',
                'category' => $this->translate("Link with responses"),
                'sortorder' => 170 , /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    'id' => gT('Only the id'),
                    'text' => gT('Only the text'),
                    'json' => gT('Both (json encoded)'),
                ),
                'default' => 'id',
                'caption' => $this->translate('Way to save values'),
            ),
        );
        $this->getEvent()->append('questionAttributes', $addLinkWithOtherReponseAttribute);
    }

    /**
     * See getPluginTwigPath event
     * remind to unsubsribe after usage
     */
    public function getPluginTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
    * get response data for twig generation
    * @param integre $sid
    * @param integer $qid
    * @param string|null $token
    * @param integer|null $srid
    * @return null|array[]
    */
    private function getData($surveyid, $qid, $token, $srid)
    {
        $oSurvey = Survey::model()->findByPk($surveyid);
        if (!$oSurvey->getIsActive()) {
            return array();
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        /* Go go */
        $aColumnToCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyid);
        $aCodeToColumn = array_flip($aColumnToCode);
        $availableColumns = SurveyDynamic::model($surveyid)->getAttributes();
        $LinkWithOtherReponseTitle = $aAttributes['LinkWithOtherReponseTitle'];
        $columnTitle = null;
        if (array_key_exists($LinkWithOtherReponseTitle, $availableColumns)) {
            $columnTitle = $value;
        } elseif (isset($aCodeToColumn[$LinkWithOtherReponseTitle])) {
            $columnTitle = $aCodeToColumn[$LinkWithOtherReponseTitle];
        }
        if (empty($columnTitle)) {
            if (Permission::model()->hasSurveyPermission($surveyid, 'surveycontent', 'read')) {
                $message = "<div class='alert alert-warning'>" . sprintf($this->translate("invalid column title %s."), CHtml::encode($LinkWithOtherReponseTitle)) . "</div>";
                $this->questionRenderEvent->set('text', $this->questionRenderEvent->get('text') . $message);
            }
            return null;
        }

        $selectColumns = array(
            'id',
            'submitdate',
            App()->getDb()->quoteColumnName($columnTitle)
        );
        $criteria = new CDBCriteria();
        if ($aAttributes['LinkWithOtherReponseSubmitted']) {
            $criteria->addCondition("submitdate IS NOT NULL");
        }
        $criteria->addCondition(App()->getDb()->quoteColumnName($columnTitle) . " IS NOT NULL AND " . App()->getDb()->quoteColumnName($columnTitle) . " <>''");
        if ($token) {
            switch ($aAttributes['LinkWithOtherReponseTokenUsage']) {
                case 'no':
                    /* must NOT happen */
                    break;
                case 'token':
                default:
                    $criteria->compare('token', $token);
                    break;
                case 'group':
                    $tokenList = \responseListAndManage\Utilities::getTokensList($surveyid, $token, false);
                    $criteria->addInCondition('token', $tokenList);
                    break;
            }
        }
        if ($srid) {
            $criteria->compare('id', "<>" . $srid);
        }

        if ($aAttributes['LinkWithOtherReponseExtraFilter']) {
            $filtersField = trim($aAttributes['LinkWithOtherReponseExtraFilter']);
            $aFieldsLines = preg_split('/\r\n|\r|\n/', $filtersField, -1, PREG_SPLIT_NO_EMPTY);
            $aFiltersFields = array();
            foreach ($aFieldsLines as $aFieldLine) {
                if (!strpos($aFieldLine, ":")) {
                    continue; // Invalid line
                }
                $key = substr($aFieldLine, 0, strpos($aFieldLine, ":"));
                $value = substr($aFieldLine, strpos($aFieldLine, ":") + 1);
                $value = \LimeExpressionManager::ProcessStepString($value, array(), 3, 1);
                if (array_key_exists($key, $availableColumns)) {
                    $aFiltersFields[$key] = $value;
                } elseif (isset($aCodeToColumn[$key])) {
                    $aFiltersFields[$aCodeToColumn[$key]] = $value;
                }
            }
            foreach ($aFiltersFields as $column => $value) {
                $criteria->compare(App()->getDb()->quoteColumnName($column), $value);
            }
        }
        $orderBy = trim($aAttributes['LinkWithOtherReponseOrderBy']);
        $sFinalOrderBy = "";
        if (!empty($orderBy)) {
            $aOrdersBy = explode(",", $orderBy);
            $aOrderByFinal = array();
            foreach ($aOrdersBy as $sOrderBy) {
                $aOrderBy = explode(" ", trim($sOrderBy));
                $arrangement = "ASC";
                if (!empty($aOrderBy[1]) and strtoupper($aOrderBy[1]) == 'DESC') {
                    $arrangement = "DESC";
                }
                if (!empty($aOrderBy[0])) {
                    $orderColumn = null;
                    if (array_key_exists($aOrderBy[0], $availableColumns)) {
                        $selectColumns[] = $aOrderBy[0];
                        $aOrderByFinal[] = App()->db->quoteColumnName($aOrderBy[0]) . " " . $arrangement;
                    } elseif (isset($aCodeToColumn[$aOrderBy[0]])) {
                        $selectColumns[] = $aCodeToColumn[$aOrderBy[0]];
                        $aOrderByFinal[] = App()->db->quoteColumnName($aCodeToColumn[$aOrderBy[0]]) . " " . $arrangement;
                    }
                }
            }
            $sFinalOrderBy = implode(",", $aOrderByFinal);
        }
        if (empty($sFinalOrderBy)) {
            $sFinalOrderBy = Yii::app()->db->quoteColumnName('id') . " DESC";
            if (Survey::model()->findByPk($surveyid)->datestamp == "Y") {
                $sFinalOrderBy = Yii::app()->db->quoteColumnName('datestamp') . " ASC";
                $selectColumns[] = 'datestamp';
            }
        }
        $criteria->select = $selectColumns;
        $criteria->order = $sFinalOrderBy;

        $oResponses = \Response::model($surveyid)->findAll($criteria);
        if (empty($oResponses)) {
            return array();
        }
        switch ($aAttributes['LinkWithOtherReponseSaveAs']) {
            case 'text':
                $aResponses = array_unique(CHtml::listData($oResponses, $columnTitle, $columnTitle));
                break;
            case 'id':
            case 'json':
            default:
                $aResponses = array_unique(CHtml::listData($oResponses, 'id', $columnTitle));
        }
        return $aResponses;
    }

    /**
     * Translate a string
     * @pmaram string $string
     * @param $string $sEscapeMode : unescaped, html, js
     * @param $string null|string $sLanguage
     * @retuirn string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($string, $sEscapeMode, $sLanguage);
    }

    /**
     * @inheritdoc
     * Adding message to vardump if user activate debug mode
     */
    public function log($message, $level = \CLogger::LEVEL_TRACE)
    {
        Yii::log("[" . get_class($this) . "] " . $message, $level, 'vardump');
        parent::log($message, $level);
    }
}
